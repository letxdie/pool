import unittest
from puul import ProcessPool, heavy_computation


class MyTestCase(unittest.TestCase):

    def test_max_count(self):
        pool = ProcessPool(min_workers=1, max_workers=13, mem_usage=30)
        result = pool.map(heavy_computation, [6])
        self.assertEqual(result, 2)

    def test_max_memory(self):
        check = ProcessPool.check(self, heavy_computation, 6)
        self.assertEqual(check, 13)

    def test_count(self):
        big_data = [6]
        pool = ProcessPool(min_workers=3, max_workers=13, mem_usage=10)
        result = pool.map(heavy_computation, big_data)
        self.assertEqual(result, None)

    def test_if_more_than_max_count(self):
        big_data = [6]
        pool = ProcessPool(min_workers=3, max_workers=13, mem_usage=230)
        result = pool.map(heavy_computation, big_data)
        self.assertEqual(int(ProcessPool().max_workers),13)


if __name__ == '__main__':
    unittest.main()
