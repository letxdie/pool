import time
import psutil
from multiprocessing import Process, Queue, current_process


def heavy_computation(data_chunk):
    for n in range(30000000):
        k = data_chunk * n ^ n


class ProcessPool:

    def __init__(self, min_workers=3, max_workers=13, mem_usage=100):  # Мегабайты
        self.min_workers = min_workers
        self.max_workers = max_workers
        self.mem_usage = mem_usage
        self.process_time = 0
        self.pick_mem_usage = 0

    def check(self, func, chunk):
        memory_list = []
        check = Process(target=func, args=(chunk,), name='check_time')
        start = time.time()
        check.start()
        check.join()
        end = time.time()
        self.process_time = int(end - start)
        check = Process(target=func, args=(chunk,), name='check_pick_memory')
        check.start()
        pid = check.pid
        for i in range(self.process_time * 10):
            mem = int(psutil.Process(pid).memory_info().rss / 1024 / 1024)
            mem += int(psutil.Process(pid).memory_full_info().swap / 1024 / 1024)
            memory_list.append(mem)
            time.sleep(0.1)
        check.join()
        self.pick_mem_usage = int((max(memory_list)))
        return self.pick_mem_usage

    def map(self, func, data):
        self.check(heavy_computation, data[0])
        q = Queue()
        process_list = []
        possible_count_of_workers = int(self.mem_usage / self.pick_mem_usage)
        if possible_count_of_workers < self.min_workers:
            return None
        if possible_count_of_workers > self.max_workers:
            count_of_workers = self.max_workers
        else:
            count_of_workers = possible_count_of_workers
        for chunk in data:
            q.put(chunk)
        while not q.empty():
            for _ in range(count_of_workers):
                data = q.get()
                process = Process(target=func, args=(data,))
                process_list.append(process)
                process.start()
                if q.empty(): break
            for proces in process_list:
                proces.join()
        return possible_count_of_workers


if __name__ == '__main__':
    big_data = [6, 2, 3, 4, 5, 6, 2, 1, 3, 2, 1, 2, 3, 1, 3, 2, 1, 3, 1, 2]
    pool = ProcessPool(min_workers=3, max_workers=13, mem_usage=100)
    result = pool.map(heavy_computation, big_data)
